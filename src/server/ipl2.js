//  Question 2 : Number of matches won per team per year in IPL

function matchesWonPerTeamPerYear(matchObj){

	let year=[...new Set (matchObj.map(x => x.season))]
	let resultantWinner=[]

    for(el of year){

    let particularYear = matchObj.filter(x=> {
        if(x.season==el){
            return x.season
        }

       
    });
    winner=particularYear.map(x=>x.winner)
  
    let objWinner= winner.reduce((acc,cur)=>{
        if(acc[cur]){
            acc[cur]=acc[cur]+1;
        }
        else{
            acc[cur]=1
        }
        return acc
    },{})
    objWinner['season']=el
	resultantWinner.push(objWinner)
    
	}

	
	console.log(resultantWinner);
	return resultantWinner;

}

module.exports = matchesWonPerTeamPerYear;

