// Create a function that converts matches.csv files to json
 const csvFilePath1='/home/shivangi/iplDrill/src/data/matches.csv'
 const csvFilePath2='/home/shivangi/iplDrill/src/data/deliveries.csv'

const csv1 = require('csvtojson')
const csv2 =  require ('csvtojson')

const matchesPerYear = require ('./ipl1.js')
const matchesWonPerTeamPerYear = require('./ipl2.js')
const extraRunsConceded = require ('./ipl3.js')
const top10EconomicalBowler = require('./ipl4.js')

const fs = require ('fs')





csv1()
.fromFile(csvFilePath1)																							//converts file matches.csv to jsonObj 
.then((jsonObj)=>{

	
	const result1  = matchesPerYear(jsonObj);																	// Question 1 : Number of matches played per year for all the years in IPL.
	fs.writeFileSync('../public/output/matchesPerYear.json',JSON.stringify(result1),"utf-8", (err) =>{
		if(err){
			throw err;
		};
	});														
																													
	const result2  = matchesWonPerTeamPerYear(jsonObj);
	fs.writeFileSync('../public/output/matchesWonPerTeamPerYear.json',JSON.stringify(result2),"utf-8", (err) =>{
		if(err){
			throw err;
		};
	});


	csv2()																										//converts file deliveries.csv to jsonObj2 
	.fromFile(csvFilePath2)
	.then((jsonObj2)=>{


	
	const result3  =	extraRunsConceded(jsonObj,jsonObj2);
	fs.writeFileSync('../public/output/extraRunsConceded.json',JSON.stringify(result3),"utf-8", (err) =>{
		if(err){
			throw err;
		};
	});

	const result4  =	top10EconomicalBowler(jsonObj,jsonObj2)
	fs.writeFileSync('../public/output/top10EconomicalBowler.json',JSON.stringify(result4),"utf-8", (err) =>{
		if(err){
			throw err;
		};
	});

			

});


		

});







































