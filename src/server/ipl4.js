//  Question 4 : Find the top 10 most economical bowler

function top10EconomicalBowler(jsonObj,jsonObj2){
    let arrayId2015FromMatches = jsonObj.filter(x=>{              										            // array 'arrayId2015FromMatches' to store id of matches played in year 2015 from matches.csv
        if(x.season=='2015')
        return x.id
    })
    .map(x=> x.id);


let arrayMatchIdFromDeliveries = jsonObj2.map(x=> x.match_id);										               //  array 'arrayOfMatchIdFromDeliveries' stores the index of jsonObj2 
let length = arrayId2015FromMatches.length;
let	x1 = arrayMatchIdFromDeliveries.indexOf(arrayId2015FromMatches[0])									
let y1 = arrayMatchIdFromDeliveries.lastIndexOf(arrayId2015FromMatches[length-1])



let bowlers=[];

for(let i=x1;i<=y1;i++){							
    bowlers.push(jsonObj2[i].bowler)
}

let bowlersListObj={};

for(let el of bowlers){
    bowlersListObj.hasOwnProperty(el) ? bowlersListObj[el]++ : bowlersListObj[el]=1
}
                                                                                                                    // uniqueBowlersList2015 stores the list of Bowlers of 2015
let uniqueBowlersList2015 =  Object.keys(bowlersListObj);	



let array2015 = [];

array2015=jsonObj2.slice();
 
array2015=array2015.slice(x1,y1)																					// array2015 stores the data of only year 2015 from deliveries

 

let totalBalls=[],runs=[],economicRate=[];
let totalRuns=0,totalOvers=0;

for(let i=0; i<uniqueBowlersList2015.length ; i++){
    

    for(let j=0; j< array2015.length ; j++){

             if(array2015[j].bowler==uniqueBowlersList2015[i]){

                totalBalls.push(array2015[j].over)
                 runs.push(array2015[j].total_runs)

            }
    }

    totalRuns = runs.reduce((a,b)=> {return +a + +b});
    totalOvers= (totalBalls.length)/6;
     
    economicRate[i]=(totalRuns/totalOvers)																
    
}
 
let objOfBowlers = {}

for(let i = 0; i< uniqueBowlersList2015.length; i++){
    
    objOfBowlers[uniqueBowlersList2015[i]] = economicRate[i];
    
}



let sortObj = [];
for (let prop in objOfBowlers) {
    sortObj.push([prop, objOfBowlers[prop]]);
}

sortObj.sort(function(a, b) {
return a[1] - b[1];
});


sortObj=sortObj.slice(0,10);


let top10EconomicalBowler2015 = sortObj.map(x => x[0]);

let top10Bowlers ={}


for(let i=0; i<top10EconomicalBowler2015.length; i++){
    top10Bowlers[i+1] = top10EconomicalBowler2015[i];
}
console.log(top10Bowlers)
return top10Bowlers
}


module.exports = top10EconomicalBowler;