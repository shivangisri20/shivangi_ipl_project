// Question 3 : Extra runs conceeded per team in 2016

function extraRunsConceded(jsonObj,jsonObj2){


    let arrayOfIdFromFileMatches = jsonObj.filter(x=>{              										 // array 'arrayOfIdFromFileMatches' to store id of matches played in year 2016 from matches.csv
        if(x.season=='2016')
        return x.id
        
    })
    .map(x=> x.id);
    

    let arrayOfMatchIdFromDeliveries = jsonObj2.map(x=> x.match_id);										//  array 'arrayOfMatchIdFromDeliveries' stores the index of jsonObj2 
    let len = arrayOfIdFromFileMatches.length;
    let	 x= arrayOfMatchIdFromDeliveries.indexOf(arrayOfIdFromFileMatches[0])									
    let y = arrayOfMatchIdFromDeliveries.lastIndexOf(arrayOfIdFromFileMatches[len-1])
            
            
    let arrayTeam=[];

    for(let i=x;i<=y;i++){							
        arrayTeam.push(jsonObj2[i].bowling_team)
    }

    let teamsOf2016={};

    for(let el of arrayTeam){
        teamsOf2016.hasOwnProperty(el) ? teamsOf2016[el]++ : teamsOf2016[el]=1
    }

    let uniqueArrayOfTeam2016 =  Object.keys(teamsOf2016);	 													//'uniqueArrayOfTeam2016' stores the list of teams played in 2016 
                                    

     

    let array2016 = [];

    array2016=jsonObj2.slice();
     
    array2016=array2016.slice(x,y)																				// array2016 stores the data of matches played in 2016 from file deliveries.csv

    let extra=[],sum=0;
    let extraRuns ={};

    for(let i=0; i<uniqueArrayOfTeam2016.length ; i++){
        extra=[],sum=0;
        for(let j=0; j< array2016.length ; j++){
                if(array2016[j].bowling_team==uniqueArrayOfTeam2016[i]){
                    extra.push(array2016[j].extra_runs)
                }
        }

        sum = extra.reduce((a,b)=> {return +a + +b});

        extraRuns[uniqueArrayOfTeam2016[i]]= sum;

       // console.log(`Extra runs conceded by team ${uniqueArrayOfTeam2016[i]} is ${sum}`);
        
    }
    console.log(extraRuns)
    return extraRuns;

}

module.exports = extraRunsConceded;